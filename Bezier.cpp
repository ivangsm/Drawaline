#include "Bezier.h"
#include "Line.h"
#include <iostream>
#include <cmath>
using namespace std;

float Bezier2(float t, int x1,int x2,int x3)
{
	float t2 = t * t;
	float mt = 1 - t;
	float mt2 = mt * mt;
	return x1 * mt2 + x2 * 2 * mt  * t + x3 * t2;
}
float Bezier3(float t, int x1, int x2, int x3, int x4)
{
	float t2 = t * t;
	float t3 = t * t*t;
	float mt = 1 - t;
	float mt2 = mt * mt;
	float mt3 = mt * mt * mt;
	return x1 * mt3 + x2 * 3 * mt2*t + x3 * 3 * mt*t2 + x4 * t3;
}
void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	int n=100000;
	SDL_RenderDrawPoint(ren, p1.x, p1.y);
	for (int i = 0; i < n; i++)
	{
		float t = float(i) / n;
		int x1 = int(Bezier2(t, p1.x, p2.x, p3.x));
		int y1 = int(Bezier2(t, p1.y, p2.y, p3.y));
		t = float(i + 1) / n;
		int x2 = int(Bezier2(t, p1.x, p2.x, p3.x));
		int y2 = int(Bezier2(t, p1.y, p2.y, p3.y));
		Bresenham_Line(x1, y1, x2, y2, ren);
		//cout << x1 << "," << y1 << endl;
		//cout << x2 << "," << y2 << endl;
	}
	SDL_RenderDrawPoint(ren, p3.x, p3.y);
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	int n = 100000;
	SDL_RenderDrawPoint(ren, p1.x, p1.y);
	for (int i = 0; i < n; i++)
	{
		float t = float(i) / n;
		int x1 = int(Bezier3(t, p1.x, p2.x, p3.x, p4.x));
		int y1 = int(Bezier3(t, p1.y, p2.y, p3.y, p4.y));
		t = float(i + 1) / n;
		int x2 = int(Bezier3(t, p1.x, p2.x, p3.x, p4.x));
		int y2 = int(Bezier3(t, p1.y, p2.y, p3.y, p4.y));
		Bresenham_Line(x1, y1, x2, y2, ren);
		//cout << x1 << "," << y1 << endl;
		//cout << x2 << "," << y2 << endl;
	}
	SDL_RenderDrawPoint(ren, p3.x, p3.y);
}


