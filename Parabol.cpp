#include "Parabol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int p, x, y;
	p = 1 - A;
	x = 0;
	y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x < A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	x = A;
	y = A / 2;
	p = 2*A - 1;
	Draw2Points(xc, yc, x, y, ren);
	
	while (y < yc)
	{
		if (p <= 0)
		{
			p += A * 4;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}
	
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{

	int p, x, y;
	p = -1 + A;
	x = 0;
	y = 0;
	Draw2Points(xc, yc, x, y, ren);
	while (x < A)
	{
		if (p <= 0)
		{
			p += -2 * x - 3 + 2 * A;
			y--;
		}
		else
		{
			p += -2 * x - 3;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	x = A;
	y = -A / 2;
	p = -2 * A + 1;
	Draw2Points(xc, yc, x, y, ren);

	while (y > -yc)
	{
		if (p <= 0)
		{
			p += -4 * A + 4 * x + 4;
			x++;
		}
		else
		{	
			p += -A * 4;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}